import sys
import re
#Module Regular Expression is imported
#Read input from STDIN (standard input)
for line in sys.stdin:
    #Remove leading and trailing whitespace
    line = line.strip()
    #Split the line into records - list of columns
    cols = line.split(',')
    #Process and output intermediate map results
    # Require 6 letters including 4 digits to match passenger id.
    if re.match("(\-{3}))\d\d\d\d$?:\w{3})", cols[1]):
        #Write the results to STDOUT (standard output)
        #Tab-delimited; Key-Value pair <Passenger_id, Flight_id>
        print '%s\t%s' % (cols[1], cols[2])
        #Output here will be the input for the reduce

