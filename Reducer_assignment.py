import sys

word = ""
current_word = ""
current_count = 0

#read input (Map_output) from STDIN
for line in sys.stdin:
    #remove leading and trailing whitespace
    line = line.strip()
    #parse input (map's tuples)
    word, count = line.split(',', 1)
    #convert count from a string into an integar
    try:
        count = int(count)
    except ValueError:
        #count is not a number, silently discard line
        continue
        #hadoop sorts map output by keyword before reduction
        #word unchanged = continue summation
        if current_word == word:
            current_count += count
            #new word encountered = Write prev. and start new count
        else:
            if current_word:
                # Write result to STDOUT
                print '%s\t%s' % (current_word, current_count)
                current_count = count
                current_word = word

#output final word if required
if current_word == word:
    print '%s\t%s' % (current_word, current_count)
